@extends('layouts.master')

    @section('content')
                     <div class="span9">
                        <div class="content">
                            <div class="btn-controls">
                                <div class="btn-box-row row-fluid">
                                    <a href="#" class="btn-box big span4"><i class=" icon-random"></i><b>65%</b>
                                        <p class="text-muted">
                                            Growth</p>
                                    </a><a href="#" class="btn-box big span4"><i class="icon-user"></i><b>15</b>
                                        <p class="text-muted">
                                            New Users</p>
                                    </a><a href="#" class="btn-box big span4"><i class="icon-money"></i><b>15,152</b>
                                        <p class="text-muted">
                                            Profit</p>
                                    </a>
                                </div>
                               
                            </div>
                        
                            <!--/.module-->
                            <div class="module hide">
                        
                            </div>
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        DataTables</h3>
                                </div>
                                <div class="module-body table">
                                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><div class="dataTables_filter" id="DataTables_Table_0_filter">
                                    </div>
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display dataTable" width="100%" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" style="width: 100%;">
                                        <thead>
                                            <tr role="row"><th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                    Rendering engine
                                                : activate to sort column descending" style="width: 134px;">
                                                    Rendering engine
                                                </th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                                                    Browser
                                                : activate to sort column ascending" style="width: 198px;">
                                                    Browser
                                                </th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                                                    Platform(s)
                                                : activate to sort column ascending" style="width: 182px;">
                                                    Platform(s)
                                                </th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                                                    Engine version
                                                : activate to sort column ascending" style="width: 116px;">
                                                    Engine version
                                                </th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="
                                                    CSS grade
                                                : activate to sort column ascending" style="width: 84px;">
                                                    CSS grade
                                                </th></tr>
                                        </thead>
                                        
                                        <tfoot>
                                            <tr><th rowspan="1" colspan="1">
                                                    Rendering engine
                                                </th><th rowspan="1" colspan="1">
                                                    Browser
                                                </th><th rowspan="1" colspan="1">
                                                    Platform(s)
                                                </th><th rowspan="1" colspan="1">
                                                    Engine version
                                                </th><th rowspan="1" colspan="1">
                                                    CSS grade
                                                </th></tr>
                                        </tfoot>
                                    <tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Firefox 1.0
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.7
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Firefox 1.5
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Firefox 2.0
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Firefox 3.0
                                                </td>
                                                <td class=" ">
                                                    Win 2k+ / OSX.3+
                                                </td>
                                                <td class="center ">
                                                    1.9
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Camino 1.0
                                                </td>
                                                <td class=" ">
                                                    OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Camino 1.5
                                                </td>
                                                <td class=" ">
                                                    OSX.3+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Netscape 7.2
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / Mac OS 8.6-9.2
                                                </td>
                                                <td class="center ">
                                                    1.7
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Netscape Browser 8
                                                </td>
                                                <td class=" ">
                                                    Win 98SE+
                                                </td>
                                                <td class="center ">
                                                    1.7
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Netscape Navigator 9
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.0
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.1
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.1
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.2
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.2
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.3
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.3
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.4
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.4
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.5
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.5
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.6
                                                </td>
                                                <td class=" ">
                                                    Win 95+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.6
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.7
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.7
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Mozilla 1.8
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.1+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Seamonkey 1.1
                                                </td>
                                                <td class=" ">
                                                    Win 98+ / OSX.2+
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Gecko
                                                </td>
                                                <td class=" ">
                                                    Epiphany 2.20
                                                </td>
                                                <td class=" ">
                                                    Gnome
                                                </td>
                                                <td class="center ">
                                                    1.8
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeC odd">
                                                <td class="  sorting_1">
                                                    KHTML
                                                </td>
                                                <td class=" ">
                                                    Konqureror 3.1
                                                </td>
                                                <td class=" ">
                                                    KDE 3.1
                                                </td>
                                                <td class="center ">
                                                    3.1
                                                </td>
                                                <td class="center ">
                                                    C
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    KHTML
                                                </td>
                                                <td class=" ">
                                                    Konqureror 3.3
                                                </td>
                                                <td class=" ">
                                                    KDE 3.3
                                                </td>
                                                <td class="center ">
                                                    3.3
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    KHTML
                                                </td>
                                                <td class=" ">
                                                    Konqureror 3.5
                                                </td>
                                                <td class=" ">
                                                    KDE 3.5
                                                </td>
                                                <td class="center ">
                                                    3.5
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr><tr class="gradeA even">
                                                <td class="  sorting_1">
                                                    Misc
                                                </td>
                                                <td class=" ">
                                                    NetFront 3.1
                                                </td>
                                                <td class=" ">
                                                    Embedded devices
                                                </td>
                                                <td class="center ">
                                                    -
                                                </td>
                                                <td class="center ">
                                                    C
                                                </td>
                                            </tr><tr class="gradeA odd">
                                                <td class="  sorting_1">
                                                    Misc
                                                </td>
                                                <td class=" ">
                                                    NetFront 3.4
                                                </td>
                                                <td class=" ">
                                                    Embedded devices
                                                </td>
                                                <td class="center ">
                                                    -
                                                </td>
                                                <td class="center ">
                                                    A
                                                </td>
                                            </tr></tbody></table>
                                </div>
                            </div>
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>

    @endsection
