<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Article Template</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
	
	<link href="https://fonts.googleapis.com/css?family=Grand+Hotel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	
	<!-- Animate.css -->

	{!! Html::style('css/frontend/animate.css') !!}
	<!-- Icomoon Icon Fonts-->
\
	{!! Html::style('css/frontend/icomoon.css') !!}
	<!-- Bootstrap  -->

	{!! Html::style('css/frontend/bootstrap.css') !!}

	<!-- Magnific Popup -->

	{!! Html::style('css/frontend/magnific-popup.css') !!}
	<!-- Flexslider  -->
	{!! Html::style('css/frontend/flexslider.css') !!}
	<!-- Owl Carousel -->
	{!! Html::style('css/frontend/owl.carousel.min.css') !!}

	{!! Html::style('css/frontend//owl.theme.default.min.css') !!}
	
	<!-- Flaticons  -->
	

	<!-- Theme style  -->

	{!! Html::style('css/frontend/style.css') !!}
	@yield('styles')
	<!-- Modernizr JS -->

	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		@include('inc.header')	
	
			@yield('content')

		@include('inc.footer')
	<!-- jQuery -->

	{!! Html::script('js/frontend/modernizr-2.6.2.min.js') !!}

	{!! Html::script('js/frontend/jquery.min.js') !!}
	<!-- jQuery Easing -->

	{!! Html::script('js/frontend/jquery.easing.1.3.js') !!}
	<!-- Bootstrap -->
	{!! Html::script('js/frontend/') !!}
	
	{!! Html::script('js/frontend/bootstrap.min.js') !!}
	
	<!-- Waypoints -->
	
	{!! Html::script('js/frontend/jquery.waypoints.min.js') !!}
	<!-- Flexslider -->

	{!! Html::script('js/frontend/jquery.flexslider-min.js') !!}
	<!-- Owl carousel -->
	
	{!! Html::script('js/frontend/owl.carousel.min.js') !!}
	<!-- Magnific Popup -->

	{!! Html::script('js/frontend/jquery.magnific-popup.min.js') !!}

	{!! Html::script('js/frontend/magnific-popup-options.js') !!}
	<!-- Main -->

	{!! Html::script('js/frontend/main.js') !!}
	@yield('scripts')
	</body>
</html>

