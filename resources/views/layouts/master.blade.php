<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        {!! Html::style('public/assets/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('public/assets/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('public/assets/bootstrap/css/bootstrap-responsive.min.css') !!}
        {!! Html::style('public/assets/css/theme.css') !!}
        {!! Html::style('public/assets/images/icons/css/font-awesome.css') !!}
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
            @yield('style')
    </head>
    <body>
       @include('inc.admin.header')
        <!-- /navbar -->
       @include('inc.admin.sidebar')
        <!--/.wrapper-->
       @yield('content')
      
         {!! Html::script('public/assets/scripts/jquery-1.9.1.min.js') !!}
         {!! Html::script('public/assets/scripts/jquery-ui-1.10.1.custom.min.js') !!}
         {!! Html::script('public/assets/bootstrap/js/bootstrap.min.js') !!}
         {!! Html::script('public/assets/scripts/flot/jquery.flot.js') !!}
         {!! Html::script('public/assets/scripts/flot/jquery.flot.resize.js') !!}
         {!! Html::script('public/assets/scripts/datatables/jquery.dataTables.js') !!}
         {!! Html::script('public/assets/scripts/common.js') !!}
        @yield('script')
    </body>
